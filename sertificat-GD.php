<?php 
// GD

header('Content-Type: image/png');

// В переменной image будет ресурс
$image = imagecreatetruecolor(500,500);

// RGB
$red = imagecolorallocate($image, 255,0,0);
$green = imagecolorallocate($image, 0,255,0);
$blue = imagecolorallocate($image, 0,0,255);
$black = imagecolorallocate($image, 0,0,0);
    
$code1 = 'Спасибо за пройденный тест Максим!';
$code2 = 'Ваша оценка:';
$code3 = '10 баллов';

// Путь к файлу
$boxFile = __DIR__ . '\download-certificate-template-png-images-transparent-gallery-advertisement-2200.png'; 
if(!file_exists($boxFile)) {
    echo 'Файл с картинкой не найден!';
    exit;
}

$imBox = imagecreatefrompng($boxFile);


// Шрифт
$fontFile = __DIR__ . '/Arial.ttf';
if(!file_exists($fontFile)) {
    echo 'Файл со шрифтом не найден!';
    exit;
}

imagefttext($imBox, 50, 0, 550, 400, $red, $fontFile, $code1);
imagettftext($imBox, 45, 0, 900, 800, $black, $fontFile, $code2);
imagettftext($imBox, 40, 0, 930, 900, $blue, $fontFile, $code3);


// Выводим изображение
imagepng($imBox);
?>